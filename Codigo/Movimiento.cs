﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour {

	public float velocidad;
	private Rigidbody rigidbody;
	private Vector3 input;
	private Vector3 movimiento;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void Update () {
		input = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
		movimiento = input * velocidad;
	}

	void FixedUpdate(){
		rigidbody.velocity = movimiento;
	}
}
